class Rating < ApplicationRecord
  belongs_to :article
  validates :score, presence: true, numericality: { greater_than: 0, less_than_or_equal_to: 5 }
end
